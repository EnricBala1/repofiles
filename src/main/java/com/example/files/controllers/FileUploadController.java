package com.example.files.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.files.components.StorageFileNotFoundException;
import com.example.files.components.StorageService;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class FileUploadController {

    @Autowired
    StorageService storageService;



    @GetMapping("/")
    public String listUploadedFiles(Model model) throws IOException {
        Stream<Path> paths = storageService.loadAll();
        Stream<String> links = paths.map(path -> {
            UriComponentsBuilder ucb = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,"serveFile", path.getFileName().toString());
            return ucb.build().toUri().toString();
        });
        List<String> lstLinks = links.collect(Collectors.toList());
        model.addAttribute("files", lstLinks);
        //model.addAttribute("files", storageService.loadAll().map(
        //        path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
        //               "serveFile", path.getFileName().toString()).build().toUri().toString())
        //        .collect(Collectors.toList()));
        return "uploadForm";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping(value = "/files/{filename:.+}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@PathVariable String filename) throws IOException {
        Resource file = storageService.loadAsResource(filename);

        InputStream is = file.getInputStream();
        byte[] img = new byte[is.available()];
        is.read(img);
        return img;
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");
        return "redirect:/";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}